
import React, { useContext, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { loadMoreContect } from '../../App'
import './Settings.scss'
import { Container } from 'reactstrap'
import 'antd/dist/antd.css';
import { Radio } from 'antd';

const Settings = () => {

    // handling function coming from the context api 
    const { loadMoreBtn, handleLoadMore } = useContext(loadMoreContect)
    // value of current active radio button
    const [value, setValue] = useState(loadMoreBtn === true ? 2 : 1)

    
    // this function will handle chabge in radio button
    const onChange = e => {
        setValue(e.target.value)
        if (e.target.value === 1) {
            handleLoadMore(false)
        }
        else {
            handleLoadMore(true)
        }
    };
    return (
        <div className="Settings">
            <div className="SettingsHeader">
                <div className="SettingsHeader_overlay">
                    <h2>Settings</h2>
                    
                </div>
            </div>
            <Container className="SettingsContent">
                <h4 className="pageTitle">Load More Settings</h4>
                <Radio.Group onChange={(e) => onChange(e)} value={value}>
                    <Radio value={1} style={{ color: value === 1 ? '#B6845C' : '#fff', fontSize: '20px' }}>Automatically</Radio>
                    <Radio value={2} style={{ color: value === 2 ? '#B6845C' : '#fff', fontSize: '20px' }}>Manually</Radio>
                </Radio.Group>
            </Container>

        </div>
    )
}

export default Settings;