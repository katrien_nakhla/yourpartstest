
import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import { API_ENDPOINT } from '../../AppConfig'
import { Link } from 'react-router-dom'
import './Explore.scss'
import { Container, Row, Col, Button } from 'reactstrap'
import phone_img_0 from '../../Assets/Images/img8.jpg'
import phone_img_1 from '../../Assets/Images/img2.webp'
import phone_img_2 from '../../Assets/Images/img5.jpg'
import phone_img_3 from '../../Assets/Images/img6.jfif'
import phone_img_4 from '../../Assets/Images/img7.webp'
import phone_img_5 from '../../Assets/Images/img8.jpg'
import Lottie from 'react-lottie';
import animationData from '../../Assets/lottiefiles/9953-loading-round.json';
import { loadMoreContect } from '../../App'


const Explore = () => {
    //this flag coming from contect api 
    // if loadMoreBtn = true  so will use the manual option
    // else will use the load on scroll option

    const { loadMoreBtn } = useContext(loadMoreContect)
    //allData wil contain all devices coming from the request
    const [allData, setAllData] = useState([])
    // subData will contain the current data show and will always increase until 
    // subData.length===allData.length
    const [subData, setSubData] = useState([])
    // to add new devices into subData array 
    const [increaseNumber, setIncreaseNumber] = useState(10)
    // this flag to open/close the loading lottie file
    const [OpenLoading, setOpenLoading] = useState(false)
    // to determin if the scroll reach to the bottom 
    // so will add new data in (subData) array
    // in the Automatically option
    const [scrollFlag, SetscrollFlag] = useState(false)

    // lottie settings 
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    useEffect(() => {
        // to start the page at the top
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
        // fetch all data from devices
        fetchAllData()
    }, [])

    useEffect(() => {
        // if the user select the automatically option
        // listen to the (scroll) event to determine the current height of the page
        window.addEventListener("scroll", function () {
            let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            let docHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
            const windowBottom = Math.round(windowHeight + window.pageYOffset);
            // console.log('height--> ',windowBottom, docHeight,docHeight-windowBottom)
            // if the scroll reach to the bottom will open the scroll falg to add new data
            if (windowBottom >= docHeight || (docHeight - windowBottom <= 5)) {
                SetscrollFlag(true)
            } else {
                if (scrollFlag === true)
                    SetscrollFlag(false)
            }
        });

    })

    useEffect(() => {
        if (scrollFlag === true && loadMoreBtn === false) {
            handleScroll()
        }
    }, [scrollFlag])

    const handleScroll = () => {
        console.log('hell0 from handleScroll ')
        // open the loading
        setOpenLoading(true)
        // adding new (10) devices
        if ((subData.length + 10) <= allData.length) {
            // console.log(subData.length, increaseNumber, allData.length)
            setSubData(allData.slice(0, increaseNumber + 10))
            setIncreaseNumber(increaseNumber + 10)
            setOpenLoading(false)
        }
    }

    const fetchAllData = () => {
        let uri = `${API_ENDPOINT}/getlatest?token=739db760403f033c631c494c81b1d51ddca3bdeed8d0e7ff&brand=Samsung`
        axios.get(uri).then(response => {
            console.log('get data done-> ', response)
            setAllData(response.data)
            setSubData(response.data.slice(0, 10))
        }).catch(error => {
            console.log('get data failes=> ', error)
        })
    }

    return (
        <div className="Explore">
            <div className="ExploreHeader">
                <div className="ExploreHeader_overlay">
                    <h2>Explore</h2>
                    
                </div>
            </div>
            <Container className="ExploreContent">
                <Row>
                    {subData.map((item, index) => {
                        return (
                            <Col lg="4" md="6" sm="6" className="mb-4 cardCol" key={index}>
                                <Link to={{
                                    pathname: '/productDetails',
                                    state: { product: item }
                                }}>
                                    <div className="SingleCard">
                                        <div className="imgPart">
                                            <img src={phone_img_0} />
                                        </div>
                                        <div className="infoPart">
                                            <h2>{item.DeviceName}</h2>
                                            <p>{item.Brand}</p>
                                        </div>
                                    </div>
                                </Link>
                            </Col>
                        )
                    })}
                </Row>

                <div>
                    {OpenLoading === true && subData.length < allData.length &&
                        <Lottie options={defaultOptions}
                            height={300}
                            width={300}
                        />
                    }
                </div>

                {loadMoreBtn === true && subData.length < allData.length &&
                    <div className="btnStyle">
                        <Button onClick={() => { handleScroll() }}>Load More</Button>
                    </div>
                }
            </Container>
        </div>
    )
}

export default Explore;