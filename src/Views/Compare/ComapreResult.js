

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import './ComapreResult.scss'
import { Container } from 'reactstrap'

const ComapreResult = (props) => {

    // will save the received table attributes at (flagsAttributes) state
    const [flagsAttributes, setflagsAttributes] = useState(props.location.state.data.flagsAttributes)
    // will save the received device 1 data at (device1) state
    const [device1, setfdevice1] = useState(props.location.state.data.device1)
    // will save the received device 2 data at (device2) state
    const [device2, setfdevice2] = useState(props.location.state.data.device2)

    return (
        <div className="CompareResult">
            <div className="CompareResultHeader">
                <div className="CompareResultHeader_overlay">
                    <h2>Compare Result</h2>
                    <p>
                        <Link to="/Compare" className="prev">Compare</Link>
                        &nbsp;/&nbsp;
                        Compare Result
                    </p>
                </div>
            </div>
            <div className="CompareResultContent">
                {flagsAttributes.length !== 0 ?
                    <Container fluid={window.outerWidth <= 757 ? true : false}>
                        <table style={{ width: '100%' }}>
                            <tr className="tableTitle">
                                <th >&nbsp;</th>
                                <th>{device1.DeviceName}</th>
                                <th>{device2.DeviceName}</th>
                            </tr>
                            {flagsAttributes.length > 0 && flagsAttributes.map((item, index) => {
                                return (
                                    <tr className={index === flagsAttributes.length - 1 ? "lastRow" : ''} >
                                        <th className="label" style={{ color: '#B6845C' }} >{item.label}</th>
                                        {/* here will check if the device contain this attribute then will show the data else will show (---) to prevfent the crashing */}
                                        <th>{device1[item.name] ? device1[item.name] : '---'}</th>
                                        <th>{device2[item.name] ? device2[item.name] : '---'}</th>
                                    </tr>
                                )
                            })}
                        </table>
                    </Container>
                    :
                    <h4 className="text-center" style={{ color: 'red' }}>You should Choose Table Attributes</h4>
                }
            </div>
        </div >
    )
}
export default ComapreResult;