import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './Compare.scss'
import { Container, Row, Col } from 'reactstrap'
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Checkbox from '@material-ui/core/Checkbox';
import { API_ENDPOINT } from '../../AppConfig'
import axios from 'axios'
import { Redirect } from 'react-router';


const Compare = (options) => {
    // flag to show / hide (the Compare Now) button
    const [showBtn, setshowBtn] = useState(false)
    // this array contain all devices data that comming from request (fetchAllDevices)                           
    const [alldevices, setalldevices] = useState([])
    // sendData will Contain the data that will forward to the next page
    const [sendData, setSendData] = useState({})
    // this a boolean flag to determine if the all process is done and the sendData is Complete successfuly then it is ready to go to next page
    const [RedirectFlag, setRedirectFlag] = useState(false)
    // Val1 will contain the result of search about device 1
    const [val1, setVal1] = useState(null)
    // Val2 will contain the result of search about device 2
    const [val2, setVal2] = useState(null)
    // array contain table attributes
    const filterAttributesFlag = [
        { label: 'Device Name', name: 'DeviceName', check: false },
        { label: 'Brand', name: 'Brand', check: false },
        { label: 'Radio', name: 'radio', check: false },
        { label: 'Speed', name: 'speed', check: false },
        { label: 'Video', name: 'video', check: false },
        { label: 'Weight', name: 'weight', check: false },
        { label: 'Bluetooth', name: 'bluetooth', check: false },
        { label: 'Colors', name: 'colors', check: false },
    ]

    useEffect(() => {
        // here will check if there are results in search about device 1 and device 2 or not
        // if yes the (Comapre Now) button will show 

        if (val1 !== null && val2 !== null) {
            setshowBtn(true)
        } else {
            setshowBtn(false)
        }
    }, [val1, val2])

    useEffect(() => {
        // to start at the top of the page smoothly
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
        // to fetch all devices
        fetchAllDevices()
    }, [])

    const fetchAllDevices = () => {

        let uri = `${API_ENDPOINT}/getlatest?token=739db760403f033c631c494c81b1d51ddca3bdeed8d0e7ff`
        axios.get(uri).then(response => {
            // will set the result of the request (all devices)  in the alldevices state
            setalldevices(response.data)
        }).catch(error => {
            //handle error
        })
    }
    const onSubmit = (values) => {
        // after the form submitted 
        // will prepare the the data that forward to the Comapre Result page
        // the send Data will Contain the choosen table attributes  and the search results 
        let flags = filterAttributesFlag.filter(item => item.check === true)

            let data = {
                flagsAttributes: flags,
                device1: val1,
                device2: val2,
            }
            setSendData(data)
            // after the data prepared successfully now will go to the next page
            setRedirectFlag(true)

        } 
    
    const handleCheckBox = (checkedFlag, index) => {
        // will change the check status of the choosen attribute
        filterAttributesFlag[index].check = checkedFlag
    }

    if (RedirectFlag === true) {
        return <Redirect to={{
            pathname: '/ComapreResult',
            state: { data: sendData }
        }} />
    }
    return (
        <div className="Compare">
            <div className="CompareHeader">
                <div className="CompareHeader_overlay">
                    <h2>Compare</h2>

                </div>
            </div>
            <Container className="CompareContent">
                <h2 className="pageTitle">Compare Between Devices</h2>

                <form
                    className="searchForm"
                    autocomplete="off">
                    <Row className="formRow">
                        <Col lg="6" md="6" sm="12" xs="12" className="formCol mb-5">
                            <Autocomplete
                                id="device1"
                                name="device1"
                                options={alldevices}
                                clearOnEscape
                                getOptionLabel={(option) => option.DeviceName}
                                autoSelect={true}
                                selectOnFocus={true}
                                // getOptionSelected={(selctedOption) => { setVal1(selctedOption) }}
                                onChange={(event, obj) => {
                                    // console.log('onChange resaa-> ', event, obj)
                                    setVal1(obj)
                                }}
                                renderInput={(params) => <TextField
                                    {...params}
                                    label="Search By Device Name"
                                    variant="outlined" />}
                            />
                        </Col>
                        <Col lg="6" md="6" sm="12" xs="12" className="formCol mb-5">

                            <Autocomplete
                                id="device2"
                                name="device2"
                                options={alldevices}
                                clearOnEscape
                                getOptionLabel={(option) => option.DeviceName}
                                // getOptionSelected={(selctedOption) => { setVal2(selctedOption) }}
                                onChange={(event, obj) => {
                                    // console.log('onChange resaa-> ', event, obj)
                                    setVal2(obj)
                                }}
                                autoSelect={true}
                                onClose={(event, reason) => { console.log("xdd", event, reason) }}
                                renderInput={(params) => <TextField
                                    {...params}
                                    label="Search By Device Name"
                                    variant="outlined" />}
                            />
                        </Col>
                    </Row>
                    <Row className="checkBoxRow">
                        <h3 className="title">Select the parameter of the table :</h3>
                        {filterAttributesFlag.map((item, index) => {
                            return (
                                <Col lg="3" md="4" sm="6">
                                    <div className="checkBOX">
                                        <Checkbox
                                            onChange={(e) => { handleCheckBox(e.target.checked, index) }}
                                            name={item.name}
                                        />
                                        {item.label}
                                    </div>
                                </Col>
                            )
                        })}
                    </Row>
                    {showBtn === true ?
                        <div className="sendBtn">
                            <Button type="button"
                                onClick={() => onSubmit()}
                            >Compare Now !</Button>
                        </div>
                        : null}
                </form>

            </Container>
        </div>
    )
}

export default Compare;