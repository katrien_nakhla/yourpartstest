import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import phone_img_0 from '../../Assets/Images/img1.jpg'

import './productDetails.scss'
import { Container, Row, Col } from 'reactstrap'

const productDetails = (props) => {

    // will receive the single product from props
        
    return (
        <div className="productDetails">
            <div className="productDetailsHeader">
                <div className="productDetailsHeader_overlay">
                    <h2>Product Details</h2>
                    <p>
                        <Link to="/Explore" className="prev">Explore</Link>
                   &nbsp;/&nbsp;
                   Product Details
                </p>
                </div>
            </div>
            <Container className="productDetailsContent">
                <Row>
                    <Col lg="6" md="6" sm="12" xs="12">
                        <div className="imgContainer">
                            <img src={phone_img_0} />
                        </div>
                    </Col>
                    <Col lg="6" md="6" sm="12" xs="12" className="info">
                        <div>
                            <h2>{props.location.state.product.DeviceName}</h2>
                            <h5><span className="infoTitle">Speed : </span>&nbsp;{props.location.state.product.speed}</h5>
                            <h5><span className="infoTitle">Bluetooth : </span>&nbsp;{props.location.state.product.bluetooth}</h5>
                            <h5><span className="infoTitle">Dimensions : </span>&nbsp;{props.location.state.product.dimensions}</h5>
                            <h5><span className="infoTitle">Battery : </span>&nbsp;{props.location.state.product.battery_c}</h5>
                            <h5><span className="infoTitle">Charging : </span>&nbsp;{props.location.state.product.charging}</h5>
                            <h5><span className="infoTitle">Chipset : </span>&nbsp;{props.location.state.product.chipset}</h5>
                            <h5><span className="infoTitle">CPU : </span>&nbsp;{props.location.state.product.cpu}</h5>
                            <h5><span className="infoTitle">Colors : </span>&nbsp;{props.location.state.product.colors}</h5>
                            <h5><span className="infoTitle">Announced : </span>&nbsp;{props.location.state.product.announced}</h5>


                        </div>

                    </Col>
                </Row>
            </Container>
        </div>
    )
}


export default productDetails;