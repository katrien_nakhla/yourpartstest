import React, { useEffect, useState } from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'
import './Header.scss'

const Header = () => {

    const [openMenu, setOpenMenu] = useState(false)

    useEffect(() => {

    }, [])
    const handleMenu = () => {
        setOpenMenu(!openMenu)
    }
    return (
        <div className="Header">
            <Container className="py-2">
                <Row>
                    <Col lg="4" md="4" sm="0" className="logoPart">
                        <Link to="/Explore" onClick={() => { setOpenMenu(false) }}>Your Parts</Link>
                        <span onClick={() => handleMenu()} className="menuIcon">Menu</span>
                    </Col>
                    <Col lg="8" md="8" sm="12" className="listCol">
                        <div className="listPart">
                            <ul>
                                <li>
                                    <Link to="/Explore" >Explore</Link>
                                    <Link to="/Compare">Compare</Link>
                                    <Link to="/Settings">Settings</Link>
                                </li>
                            </ul>
                        </div>
                    </Col>
                </Row>
                {openMenu === true ?
                    <div className="Menu">
                        <ul>
                            <li>
                                <Link to="/Explore" onClick={() => { setOpenMenu(false) }}>Explore</Link>
                                <Link to="/Compare" onClick={() => { setOpenMenu(false) }}>Compare</Link>
                                <Link to="/Settings" onClick={() => { setOpenMenu(false) }}>Settings</Link>
                            </li>
                        </ul>
                    </div>
                    : null
                }
            </Container>

        </div>
    )
}

export default Header;