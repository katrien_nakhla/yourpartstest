import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './Components/Header/Header';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from './routes'
import cookie from 'react-cookies'


const PublicRoute = ({ component: Component, idx, path, exact, name }) => (
  <Route key={idx} path={path} exact={exact} name={name}
    render={props => <Component {...props} />
    }
  />
)

export const loadMoreContect = React.createContext()
export const DarkModeContext = React.createContext();

function App() {

  // if loadMoreBtn = false will use the another option load on scroll
  const [loadMoreBtn, setLoadMoreBtn] = useState(cookie.load('loadingOption') === undefined || cookie.load('loadingOption') === "manual" ? true : false)

  // this function will change the loadMoreBtn state true/fasle
  const handleLoadMore = (flag) => {
    setLoadMoreBtn(flag)
    if (flag === true) {
      cookie.save('loadingOption', 'manual', { path: '/' })
    } else {
      cookie.save('loadingOption', 'auto', { path: '/' })
    }
  }

  return (
    // loadMoreContect will share will all pages 2 parameters 
    // 1-> the status of loadMoreBtn to know which approach will use in explore page
    // 2-> the handling function that will use in  setting page to chage the current state of loadMoreBtn
    <loadMoreContect.Provider value={{ loadMoreBtn: loadMoreBtn, handleLoadMore: handleLoadMore }}>
      <DarkModeContext.Provider>
        <div className="App">
          <BrowserRouter>
            <Header />
            <Switch >
              {routes.map((route, idx) => {
                return (
                  <PublicRoute path={route.path}
                    idx={idx}
                    exact={route.path}
                    name={route.name}
                    component={route.component} />
                )
              },
              )}
            </Switch>
          </BrowserRouter>
        </div>
      </DarkModeContext.Provider>
    </loadMoreContect.Provider>
  );

}

export default App;
