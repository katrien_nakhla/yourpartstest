import Loadable from 'react-loadable'
import React from 'react'


function Loading() {
    return <div className="loading">

    </div>;
}



// Explore
const Explore = Loadable({
    loader: () => import('./Views/Explore/Explore'),
    loading: Loading,
});
// Compare
const Compare = Loadable({
    loader: () => import('./Views/Compare/Compare'),
    loading: Loading,
});
const ComapreResult = Loadable({
    loader: () => import('./Views/Compare/ComapreResult'),
    loading: Loading,
});
const Settings = Loadable({
    loader: () => import('./Views/Settings/Settings'),
    loading: Loading,
});

const productDetails = Loadable({
    loader: () => import('./Views/productDetails/productDetails'),
    loading: Loading,
});
const routes = [
    { path: `/Explore`, exact: true, name: 'Explore', component: Explore },
    { path: `/Compare`, exact: true, name: 'Compare', component: Compare },
    { path: `/ComapreResult`, exact: true, name: 'ComapreResult', component: ComapreResult },
    { path: `/Settings`, exact: true, name: 'Settings', component: Settings },
    { path: `/productDetails`, exact: true, name: 'productDetails', component: productDetails },


]

export default routes;